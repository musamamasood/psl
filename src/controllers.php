<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
/* Define constants for app */
define("API_KEY", "e9d1353322f57380fc0dc2e699144595");
define("API_SECRET", "feb47cdb251a0e45dc8ceee90986606a");
define("APP_URL", "https://3f62db27.ngrok.io");
//Request::setTrustedProxies(array('127.0.0.1'));
$app->get('/', function () {
    return 1;
});


/*
 * {
  "timeTable": [
    {
      "date": "Thursday, February 22",
      "matches": [
        {
          "0": "Peshawar Zalmi vs Multan Sultans"
        }
      ],
      "time": [
        {
          "0": "10:00 pm"
        }
      ],
      "venue": "Dubai"
    },
    {
      "date": "Friday, February 23",
      "matches": [
        {
          "0": "Karachi Kings vs Quetta Gladiators"
        },{
          "0": "Multan Sultans vs Lahore Qalandars"
        }
      ],
      "time": [
        {
          "0": "4:30 pm"
        },{
          "0": "9:00 pm"
        }
      ],
      "venue": "Dubai"
    },
    {
      "date": "Friday, February 24",
      "matches": [
        {
          "0": "Islamabad United vs Peshawar Zalmi"
        },{
          "0": "Quetta Gladiators vs Lahore Qalandars"
        }
      ],
      "time": [
        {
          "0": "4:30 pm"
        },{
          "0": "9:00 pm"
        }
      ],
      "venue": "Dubai"
    },
    {
      "date": "Sunday, February 25",
      "matches": [
        {
          "0": "Multan Sultans vs Islamabad United"
        },{
          "0": "Karachi Kings vs Peshawar Zalmi"
        }
      ],
      "time": [
        {
          "0": "4:30 pm"
        },{
          "0": "9:00 pm"
        }
      ],
      "venue": "Dubai"
    },
    {
      "date": "Monday, February 26",
      "matches": [
        {
          "0": "Karachi Kings vs Lahore Qalandars"
        }
      ],
      "time": [
        {
          "0": "9:00 pm"
        }
      ],
      "venue": "Dubai"
    },
    {
      "date": "Wednesday, February 28",
      "matches": [
        {
          "0": "Islamabad United vs Quetta Gladiators"
        }
      ],
      "time": [
        {
          "0": "9:00 pm"
        }
      ],
      "venue": "Sharjah"
    },
    {
      "date": "Thursday, March 1",
      "matches": [
        {
          "0": "Quetta Gladiators vs Peshawar Zalmi"
        }
      ],
      "time": [
        {
          "0": "9:00 pm"
        }
      ],
      "venue": "Sharjah"
    },
    {
      "date": "Friday, March 2",
      "matches": [
        {
          "0": "Multan Sultans vs Karachi Kings"
        },{
          "0": "Lahore Qalandars vs Islamabad United"
        }
      ],
      "time": [
        {
          "0": "4:30 pm"
        },{
          "0": "9:00 pm"
        }
      ],
      "venue": "Sharjah"
    },
    {
      "date": "Saturday, March 3",
      "matches": [
        {
          "0": "Multan Sultans vs Quetta Gladiators"
        },{
          "0": "Peshawar Zalmi vs Lahore Qalandars"
        }
      ],
      "time": [
        {
          "0": "4:30 pm"
        },{
          "0": "9:00 pm"
        }
      ],
      "venue": "Sharjah"
    },
    {
      "date": "Sunday, March 4",
      "matches": [
        {
          "0": "Islamabad United vs Karachi Kings"
        }
      ],
      "time": [
        {
          "0": "9:00 pm"
        }
      ],
      "venue": "Sharjah"
    },
    {
      "date": "Tuesday March 6",
      "matches": [
        {
          "0": "Peshawar Zalmi vs Multan Sultans"
        }
      ],
      "time": [
        {
          "0": "9:00 pm"
        }
      ],
      "venue": "Dubai"
    },
    {
      "date": "Wednesday, March 7",
      "matches": [
        {
          "0": "Multan Sultans vs Quetta Gladiators"
        }
      ],
      "time": [
        {
          "0": "9:00 pm"
        }
      ],
      "venue": "Dubai"
    },
    {
      "date": "Thursday, March 8",
      "matches": [
        {
          "0": "Islamabad United vs Lahore Qalandars"
        },{
          "0": "Karachi Kings vs Quetta Gladiators"
        }
      ],
      "time": [
        {
          "0": "4:30 pm"
        },{
          "0": "9:00 pm"
        }
      ],
      "venue": "Dubai"
    },
    {
      "date": "Thursday, March 8",
      "matches": [
        {
          "0": "Islamabad United vs Lahore Qalandars"
        },{
          "0": "Karachi Kings vs Quetta Gladiators"
        }
      ],
      "time": [
        {
          "0": "4:30 pm"
        },{
          "0": "9:00 pm"
        }
      ],
      "venue": "Dubai"
    },
    {
      "date": "Friday, March 9",
      "matches": [
        {
          "0": "Multan Sultans vs Lahore Qalandars"
        },{
          "0": "Peshawar Zalmi vs Islamabad United"
        }
      ],
      "time": [
        {
          "0": "4:30 pm"
        },{
          "0": "9:00 pm"
        }
      ],
      "venue": "Dubai"
    },
    {
      "date": "Saturday, March 10",
      "matches": [
        {
          "0": "Multan Sultans vs Karachi Kings"
        },{
          "0": "Peshawar Zalmi vs Quetta Gladiators"
        }
      ],
      "time": [
        {
          "0": "4:30 pm"
        },{
          "0": "9:00 pm"
        }
      ],
      "venue": "Dubai"
    },
    {
      "date": "Sunday, March 11",
      "matches": [
        {
          "0": "Karachi Kings vs Lahore Qalandars"
        }
      ],
      "time": [
        {
          "0": "9:00 pm"
        }
      ],
      "venue": "Dubai"
    },
    {
      "date": "Tuesday March 13",
      "matches": [
        {
          "0": "Multan Sultans vs Islamabad United"
        }
      ],
      "time": [
        {
          "0": "9:00 pm"
        }
      ],
      "venue": "Sharjah"
    },
    {
      "date": "Wednesday, March 14",
      "matches": [
        {
          "0": "Quetta Gladiators vs Lahore Qalandars"
        }
      ],
      "time": [
        {
          "0": "9:00 pm"
        }
      ],
      "venue": "Sharjah"
    },
    {
      "date": "Thursday, March 15",
      "matches": [
        {
          "0": "Peshawar Zalmi vs Karachi Kings"
        },{
          "0": "Quetta Gladiators vs Islamabad United"
        }
      ],
      "time": [
        {
          "0": "4:30 pm"
        },{
          "0": "9:00 pm"
        }
      ],
      "venue": "Sharjah"
    },
    {
      "date": "Friday, March 16",
      "matches": [
        {
          "0": "Peshawar Zalmi vs Lahore Qalandars"
        },{
          "0": "Islamabad United vs Karachi Kings"
        }
      ],
      "time": [
        {
          "0": "4:30 pm"
        },{
          "0": "9:00 pm"
        }
      ],
      "venue": "Sharjah"
    }
  ]
}*/